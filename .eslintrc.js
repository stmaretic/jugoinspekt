module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["prettier"],
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    sourceType: "module"
  },
  plugins: ["react", "prettier"],
  rules: {
    rules: {
      "prettier/prettier": "error"
    },
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    semi: ["error", "always"]
  }
};
