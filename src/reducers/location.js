import { FETCH_LOCATION } from "../actions/actionTypes";

const locationReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_LOCATION:
      const { latitude, longitude, accuracy } = action.payload.coords;
      return {
        ...state,
        lat: latitude,
        lng: longitude,
        accuracy,
        requestCompleted: true
      };
    default:
      return state;
  }
};

export default locationReducer;
