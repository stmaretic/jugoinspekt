import {
  ADD_MORE_INPUTS,
  REMOVE_INPUT,
  ADD_MEASUREMENT,
  PAGE_NEXT,
  PAGE_BACK,
  GOTO_MEASUREMENT,
  SAVE_MEASUREMENT_DATA,
  SAVE_CONTROL_FORM_DATA
} from "../actions/actionTypes";

const initialState = {
  currentMeasurementPage: 0,
  measurements: [
    ["parameter_1", "parameter_2", "parameter_3", "parameter_4", "parameter_5"]
  ],
  formId: "control-measurement-1",
  control: [],
  data: []
};

const lists = (state = initialState, action) => {
  const { measurements, currentMeasurementPage } = state;

  switch (action.type) {
    case ADD_MEASUREMENT:
      const newArray = [
        [
          "parameter_1",
          "parameter_2",
          "parameter_3",
          "parameter_4",
          "parameter_5"
        ]
      ];

      const measurementArray = measurements.slice(0).concat(newArray);
      return {
        ...state,
        formId: action.payload,
        currentMeasurementPage: measurements.length,
        measurements: measurementArray
      };
    case SAVE_MEASUREMENT_DATA:
      let newData = state.data.slice(0);
      newData[currentMeasurementPage] = action.payload;
      return {
        ...state,
        data: newData
      };
    case SAVE_CONTROL_FORM_DATA:
      let newControl = state.control.slice(0).concat(action.payload);
      return { ...state, control: newControl };
    case ADD_MORE_INPUTS:
      console.log(action.payload);
      let newMeasurements = measurements.slice(0);
      newMeasurements[currentMeasurementPage] = newMeasurements[
        currentMeasurementPage
      ].slice(0);
      newMeasurements[currentMeasurementPage].push(action.payload);
      return {
        ...state,
        measurements: newMeasurements
      };
    case REMOVE_INPUT:
      let newList = measurements.slice(0);
      newList[currentMeasurementPage] = newList[currentMeasurementPage].slice(
        0
      );
      newList[currentMeasurementPage].splice(action.id, 1);
      return {
        ...state,
        measurements: newList
      };
    case PAGE_NEXT:
      if (currentMeasurementPage + 1 < measurements.length) {
        return {
          ...state,
          currentMeasurementPage: currentMeasurementPage + 1
        };
      }
      return state;
    case PAGE_BACK:
      if (currentMeasurementPage > 0) {
        return {
          ...state,
          currentMeasurementPage: currentMeasurementPage - 1
        };
      }
      return state;
    case GOTO_MEASUREMENT:
      return { ...state, currentMeasurementPage: action.payload };
    default:
      return state;
  }
};

export default lists;
