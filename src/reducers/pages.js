import {
  FORM_REDIRECT_SUBMIT_PAGE,
  NEXT_PAGE,
  PREV_PAGE,
  FIRST_PAGE,
  GOTO_MEASUREMENT
} from "../actions/actionTypes";

const pagesReducer = (state = { page: 1 }, action) => {
  switch (action.type) {
    case FORM_REDIRECT_SUBMIT_PAGE:
      return { ...state, page: 0 };
    case NEXT_PAGE:
      return { ...state, page: state.page + 1 };
    case PREV_PAGE:
      return { ...state, page: state.page - 1 };
    case FIRST_PAGE:
      return { ...state, page: 1 };
    case GOTO_MEASUREMENT:
      return { ...state, page: 2 };
    default:
      return state;
  }
};

export default pagesReducer;
