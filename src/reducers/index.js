import { combineReducers } from "redux";

import formReducer from "./form";
import authReducer from "./auth";
import locationReducer from "./location";
import listsReducer from "./lists";
import pagesReducer from "./pages";
import measurementsReducer from "./measurements";

const rootReducer = combineReducers({
  auth: authReducer,
  form: formReducer,
  location: locationReducer,
  lists: listsReducer,
  pages: pagesReducer,
  measurements: measurementsReducer
});

export default rootReducer;
