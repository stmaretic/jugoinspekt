import {
  DROPDOWN_MEASUREMENT,
  SAVE_MEASUREMENT_DATA
} from "../actions/actionTypes";

const initialState = {
  activeDropdownMeasurement: null,
  formData: []
};

const measurements = (state = initialState, action) => {
  switch (action.type) {
    case DROPDOWN_MEASUREMENT:
      return { ...state, activeDropdownMeasurement: action.payload };
    default:
      return state;
  }
};

export default measurements;
