import {
  LOGIN_REQUEST,
  LOGIN_SUCCESSFUL,
  LOGOUT_USER,
  LOGOUT_SUCCESSFUL,
  AUTH_ERROR,
  REDIRECT_UNAUTHORIZED_USER,
  REDIRECT_SUCCESSFUL,
  REDIRECT_AUTH_USER
} from "../actions/actionTypes";

const auth = (state = {}, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        completed: false,
        userToken: action.payload.csrf_token
      };
    case LOGIN_SUCCESSFUL:
      return { ...state, completed: true };
    case LOGOUT_USER:
      if (action.payload) {
        return { ...state, completed: false, userToken: null };
      }
      return state;
    case LOGOUT_SUCCESSFUL:
      return { ...state, completed: true };
    case AUTH_ERROR:
      return { ...state, error: action.payload };
    case REDIRECT_UNAUTHORIZED_USER:
      return { ...state, redirect: { completed: false } };
    case REDIRECT_AUTH_USER:
      return { ...state, redirect: { completed: false } };
    case REDIRECT_SUCCESSFUL:
      return { ...state, redirect: { completed: true } };
    default:
      return state;
  }
};

export default auth;
