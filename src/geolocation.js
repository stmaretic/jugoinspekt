const geoSuccess = position => {
  return {
    lat: position.coords.latitude,
    lng: position.coords.longitude
  };
};

const geoError = () => {
  console.log("Couldn't get geolocation.");
};

export function geolocation() {
  try {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
    }
  } catch (error) {
    console.log(error.toString());
  }
}

export function watchLocation() {
  try {
    if ("geolocation" in navigator) {
      navigator.geolocation.watchPosition(geoSuccess, geoError);
    }
  } catch (error) {
    console.log(error.toString());
  }
}

export function clearWatch(id) {
  navigator.geolocation.clearWatch(id);
}
