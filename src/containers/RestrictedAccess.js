import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { redirectAuthUser } from "../actions";

/*
  HOC that restricts access to certain routes.

  If user is logged in restricts access to pages he wouldn't
  see if he was a guest, 
  example: Redirect if logged in user tried to visit login.
*/

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
      router: PropTypes.object
    };

    componentWillMount() {
      if (this.props.token) {
        // this.context.router.push("/login");
        this.props.redirectAuthUser();
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.token) {
        // this.context.router.push("/login");
        this.props.redirectAuthUser();
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    token: state.auth.userToken
  });

  const mapDispatchToProps = {
    redirectAuthUser
  };

  return connect(mapStateToProps, mapDispatchToProps)(Authentication);
}
