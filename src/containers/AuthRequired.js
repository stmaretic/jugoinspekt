import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { redirectUnauthUser } from "../actions";

/*
  HOC that wraps a route.

  Redirect an unauthenticated user if he tries visiting pages
  he's not allowed to as a guest.
*/

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
      router: PropTypes.object
    };

    componentWillMount() {
      if (!this.props.token) {
        // this.context.router.push("/login");
        this.props.redirectUnauthUser();
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.token) {
        // this.context.router.push("/login");
        this.props.redirectUnauthUser();
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    token: state.auth.userToken
  });

  const mapDispatchToProps = {
    redirectUnauthUser
  };

  return connect(mapStateToProps, mapDispatchToProps)(Authentication);
}
