import React, { Component } from "react";
import { Link } from "react-router-dom";

import AuthButtons from "./AuthButtons";
import ControlButton from "./ControlButton";

class NavbarLinks extends Component {
  render() {
    const { clicked } = this.props;
    const burgerClass = `navbar-menu ${clicked ? "is-active" : ""}`;
    return (
      <div className={burgerClass}>
        <Link to="/" className="navbar-item">
          Glavna stranica
        </Link>
        <ControlButton />
        <Link to="/contact" className="navbar-item">
          Kontakt
        </Link>
        <div className="navbar-end">
          <AuthButtons />
        </div>
      </div>
    );
  }
}

export default NavbarLinks;
