import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { logoutUser } from "../../actions";

class AuthButtons extends Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.logoutUser(this.props.token);
  }

  render() {
    if (!this.props.token) {
      return (
        <Link to="/login" className="navbar-item">
          ULOGUJTE SE
        </Link>
      );
    } else {
      return (
        <a onClick={this.onClick} className="navbar-item">
          IZLOGUJTE SE
        </a>
      );
    }
  }
}

const mapStateToProps = state => ({
  token: state.auth.userToken
});

const mapDispatchToProps = {
  logoutUser
};

AuthButtons.propTypes = {
  token: PropTypes.string,
  logoutUser: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthButtons);
