import React, { Component } from "react";

import NavbarLinks from "./NavbarLinks";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      burgerClicked: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { burgerClicked } = this.state;
    !burgerClicked
      ? this.setState({ burgerClicked: true })
      : this.setState({ burgerClicked: false });
  }

  render() {
    const { burgerClicked } = this.state;
    const burgerButtonClass = `button navbar-burger ${
      burgerClicked ? "is-active" : ""
    }`;

    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            JUGOINSPEKT
          </a>

          <button onClick={this.handleClick} className={burgerButtonClass}>
            <span />
            <span />
            <span />
          </button>
        </div>

        <NavbarLinks clicked={this.state.burgerClicked} />
      </nav>
    );
  }
}

export default Navbar;
