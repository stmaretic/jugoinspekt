import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class ControlButton extends Component {
  render() {
    return (
      (this.props.token && (
        <Link to="/control" className="navbar-item">
          Kontrola
        </Link>
      )) ||
      null
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.userToken
});

export default connect(mapStateToProps)(ControlButton);
