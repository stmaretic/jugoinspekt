import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { pageBack, pageNext } from "../../actions/lists";

import FormNavigationButton from "./FormNavigationButton";

class FormNavigation extends Component {
  handleBack(e) {
    e.preventDefault();
    this.props.pageBack();
  }

  handleNext(e) {
    e.preventDefault();
    this.props.pageNext();
  }

  render() {
    return (
      <div style={{ marginTop: 30 }} className="container">
        <div className="columns is-centered">
          <FormNavigationButton
            pos="left"
            onClick={this.handleBack.bind(this)}
          />
          <h2 className="has-text-centered">MERENJE {this.props.active + 1}</h2>
          <FormNavigationButton
            pos="right"
            onClick={this.handleNext.bind(this)}
          />
        </div>
      </div>
    );
  }
}

FormNavigation.propTypes = {
  active: PropTypes.number.isRequired,
  pageBack: PropTypes.func.isRequired,
  pageNext: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  active: state.lists.currentMeasurementPage
});

const mapDispatchToProps = {
  pageBack,
  pageNext
};

export default connect(mapStateToProps, mapDispatchToProps)(FormNavigation);
