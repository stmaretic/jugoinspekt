import React, { Component } from "react";
import { connect } from "react-redux";
import { nextPage } from "../../actions/pages";
import { saveControlData } from "../../actions/lists";

import FormControl from "./FormControl";
import FormMeasurement from "./FormMeasurement";
import FormSubmit from "./FormSubmit";

class MultiPageForm extends Component {
  render() {
    const { nextPage, page } = this.props;
    return (
      <div className="container">
        <div className="columns">
          {page === 1 && (
            <FormControl
              saveControlData={this.props.saveControlData}
              nextPage={nextPage}
            />
          )}
          {this.props.measurements &&
            page >= 2 && (
              <FormMeasurement
                nextPage={nextPage}
                // fields={this.props.measurements}
              />
            )}
          {page === 0 && <FormSubmit />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentMeasurementPage: state.lists.currentMeasurementPage,
  measurements: state.lists.measurements[state.lists.currentMeasurementPage],
  page: state.pages.page
});

const mapDispatchToProps = {
  nextPage,
  saveControlData
};

export default connect(mapStateToProps, mapDispatchToProps)(MultiPageForm);
