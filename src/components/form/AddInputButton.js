import React from "react";
import { connect } from "react-redux";
import { addMoreInputs } from "../../actions/lists";

const AddInputButton = ({ addMoreInputs }) => {
  return (
    <a style={{ display: "block" }} onClick={addMoreInputs}>
      + Još parametara
    </a>
  );
};

const mapDispatchToProps = {
  addMoreInputs
};

export default connect(null, mapDispatchToProps)(AddInputButton);
