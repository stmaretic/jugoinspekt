import React from "react";
import { connect } from "react-redux";
import { addMeasurement } from "../../actions/lists";

const AddMeasurementButton = ({ addMeasurement }) => {
  return (
    <button
      type="submit"
      onClick={e => {
        e.preventDefault();
        addMeasurement();
      }}
      className="button is-outline"
    >
      NOVO MERENJE
    </button>
  );
};

const mapDispatchToProps = {
  addMeasurement
};

export default connect(null, mapDispatchToProps)(AddMeasurementButton);
