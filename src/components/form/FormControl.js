import React, { Component } from "react";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import { saveControlData } from "../../actions/lists";

import DatePicker from "react-datepicker";
import FormInput from "./FormInput";

class FormControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment()
    };

    this.onChange = this.onChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChange(date) {
    this.setState({
      startDate: date
    });
  }

  onSelect() {}

  handleSubmit(e) {
    e.preventDefault();
    const formValues = document.getElementById("control").elements;
    let data = {};
    for (let i = 0; i < formValues.length; i++) {
      // (formValues[i].type = "text" &&
      if (formValues[i].value) {
        data[formValues[i].name] = formValues[i].value;
      }
    }
    this.props.saveControlData(data);
    this.props.nextPage();
  }

  render() {
    return (
      <div className="container has-text-centered">
        <h2>Kontrola</h2>
        <form id="control" onSubmit={this.handleSubmit}>
          <div className="columns">
            <div className="column">
              <FormInput name="entry" type="text" placeholder="Ulaz" />
            </div>
            <div className="column">
              <DatePicker
                selected={this.state.startDate}
                onChange={this.onChange}
                onSelect={this.onSelect}
                showTimeSelect
                dateFormat="LLL"
                className="input"
              />
            </div>
          </div>
          <FormInput name="controlID" type="text" placeholder="Broj kontrole" />
          <FormInput
            name="controlLocation"
            type="text"
            placeholder="Mesto kontrole"
          />
          <FormInput name="orderer" type="text" placeholder="Nalogodavac" />
          <FormInput name="product" type="text" placeholder="Proizvod" />
          <button type="submit" className="button is-outline">
            DALJE
          </button>
        </form>
      </div>
    );
  }
}

export default FormControl;
