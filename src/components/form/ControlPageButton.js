import React from "react";
import { connect } from "react-redux";
import { firstPage } from "../../actions/pages";

const ControlPageButton = ({ firstPage }) => {
  return (
    <div className="is-pulled-right">
      <a className="button is-outline" onClick={firstPage}>
        <i className="fas fa-cog" />Podešavanje kontrole
      </a>
    </div>
  );
};

const mapDispatchToProps = {
  firstPage
};

export default connect(null, mapDispatchToProps)(ControlPageButton);
