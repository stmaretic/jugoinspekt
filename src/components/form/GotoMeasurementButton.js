import React from "react";
import { connect } from "react-redux";
import { gotoMeasurement } from "../../actions/measurements";

const GotoMeasurementButton = ({ gotoMeasurement, index }) => {
  return <a onClick={() => gotoMeasurement(index)}>Izmeni</a>;
};

const mapDispatchToProps = {
  gotoMeasurement
};

export default connect(null, mapDispatchToProps)(GotoMeasurementButton);
