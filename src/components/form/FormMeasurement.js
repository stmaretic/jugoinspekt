import React, { Component } from "react";
import { connect } from "react-redux";

import FormNavigation from "./FormNavigation";
import FormInput from "./FormInput";
import ControlPageButton from "./ControlPageButton";
import AddMeasurementButton from "./AddMeasurementButton";
import SaveMeasurementButton from "./SaveMeasurementButton";
import AddInputButton from "./AddInputButton";
import { saveMeasurement } from "../../actions/measurements";

class FormMeasurements extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.placeholder = this.placeholder.bind(this);
  }

  /*
  Returns the name of the input in this format: "Parameter n",
  where n is the index of the parameter for the active measurement.
  */
  placeholder(name, index) {
    const parameter = name.split("_");
    return (
      parameter[0].charAt(0).toUpperCase() +
      parameter[0].slice(1) +
      " " +
      (index + 1)
    );
  }

  handleSubmit(e) {
    e.preventDefault();
    const formValues = document.getElementById(this.props.formId).elements;
    let data = {};
    for (let i = 0; i < formValues.length; i++) {
      if ((formValues[i].type = "text" && formValues[i].value)) {
        data[formValues[i].name] = formValues[i].value;
      }
    }
    this.props.saveMeasurement(data);
  }

  render() {
    const { formId } = this.props;
    console.log(this.props.fields);
    return (
      <div className="container">
        <FormNavigation />
        <form id={formId} onSubmit={this.handleSubmit}>
          {this.props.fields.map((field, index) => {
            return (
              <FormInput
                key={index}
                index={index}
                name={field}
                placeholder={this.placeholder(field, index)}
                hasDelete="true"
              />
            );
          })}
          <AddInputButton />
          <button type="submit" className="button is-outline">
            SAČUVAJ
          </button>
          <SaveMeasurementButton />
          <AddMeasurementButton />
          <ControlPageButton />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  formId: state.lists.formId,
  fields: state.lists.measurements[state.lists.measurements.length - 1]
});

const mapDispatchToProps = {
  saveMeasurement
};

export default connect(mapStateToProps, mapDispatchToProps)(FormMeasurements);
