import React from "react";
import { connect } from "react-redux";

import DropdownMeasurementButton from "./DropdownMeasurementButton";
import GotoMeasurementButton from "./GotoMeasurementButton";
import FormSubmitListItem from "./FormSubmitListItem";

const FormSubmitList = ({ data, dropdownMeasurement, active }) => {
  console.log(active);
  return data.map((measurement, index) => {
    const kvPairs = Object.entries(measurement);
    return (
      <ul key={index}>
        <DropdownMeasurementButton index={index} />
        {active === index && (
          <div>
            <ul>
              <hr />
              {kvPairs.map((field, index) => {
                return (
                  <FormSubmitListItem key={index} field={field} index={index} />
                );
              })}
            </ul>
            <hr />
            <GotoMeasurementButton index={index} />
            <hr />
          </div>
        )}
      </ul>
    );
  });
};

const mapStateToProps = state => ({
  data: state.lists.data,
  active: state.measurements.activeDropdownMeasurement
});

export default connect(mapStateToProps)(FormSubmitList);
