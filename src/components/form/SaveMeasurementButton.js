import React from "react";
import { connect } from "react-redux";
import { redirectSubmitPage } from "../../actions/pages";

const SaveMeasurementButton = ({ onClick, redirectSubmitPage }) => {
  return (
    <button className="button is-outline" onClick={redirectSubmitPage}>
      ZAVRSI
    </button>
  );
};

const mapDispatchToProps = {
  redirectSubmitPage
};

export default connect(null, mapDispatchToProps)(SaveMeasurementButton);
