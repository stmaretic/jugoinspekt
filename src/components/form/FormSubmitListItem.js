import React from "react";

const FormSubmitListItem = ({ field, index }) => {
  return (
    <li key={index}>
      Parametar {index + 1}: {field[1]}
    </li>
  );
};

export default FormSubmitListItem;
