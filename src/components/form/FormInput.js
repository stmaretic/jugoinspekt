import React from "react";

import DeleteButton from "./DeleteButton";

const FormInput = ({ index, type, name, placeholder, hasDelete }) => {
  return (
    <div className="field">
      <div className="control">
        <input
          type={type}
          name={name}
          className="input"
          placeholder={placeholder}
        />
        {hasDelete && <DeleteButton index={index} />}
      </div>
    </div>
  );
};

export default FormInput;
