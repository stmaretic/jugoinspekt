import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { removeInput } from "../../actions/lists";

const DeleteButton = ({ removeInput, index }) => {
  return (
    <figure
      onClick={() => {
        removeInput(index);
      }}
    >
      <i className="fas fa-minus-circle" />
    </figure>
  );
};

const mapDispatchToProps = {
  removeInput
};

DeleteButton.propTypes = {
  removeInput: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
};

export default connect(null, mapDispatchToProps)(DeleteButton);
