import React from "react";

import ControlPageButton from "./ControlPageButton";
import FormSubmitList from "./FormSubmitList";

const onSubmit = () => {};

const FormSubmit = ({ data }) => {
  return (
    <form className="container is-fluid" onSubmit={onSubmit}>
      <button className="button" type="submit">
        Zatvori lokaciju
      </button>
      <FormSubmitList />
      <ControlPageButton />
    </form>
  );
};

export default FormSubmit;
