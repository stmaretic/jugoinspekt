import React from "react";
import { connect } from "react-redux";
import { dropdownMeasurement } from "../../actions/measurements";

const DropdownMeasurementButton = ({ dropdownMeasurement, index }) => {
  return (
    <a style={{ display: "block" }} onClick={() => dropdownMeasurement(index)}>
      {`Merenje ${index + 1}`}
    </a>
  );
};

const mapDispatchToProps = {
  dropdownMeasurement
};

export default connect(null, mapDispatchToProps)(DropdownMeasurementButton);
