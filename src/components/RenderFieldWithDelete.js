import React from "react";

import DeleteButton from "./DeleteButton";

const RenderFieldWithDelete = (
  {
    input,
    name,
    className,
    placeholder,
    index,
    type,
    meta: { touched, error }
  },
  ...props
) => {
  const inputClassName = touched && error ? "input is-danger" : className;

  return (
    <div className="field">
      <div className="control">
        <input
          {...input}
          className={inputClassName}
          type={type}
          placeholder={placeholder}
        />
        <DeleteButton index={index} />
        {touched &&
          error && (
            <span
              className="has-text-danger"
              style={{ display: "inline-block", margin: "4px 20px" }}
            >
              {error}
            </span>
          )}
      </div>
    </div>
  );
};

export default RenderFieldWithDelete;
