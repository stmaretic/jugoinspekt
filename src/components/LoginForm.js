import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import { loginUser } from "../actions";
import { connect } from "react-redux";
import { required } from "../formValidation";

import RenderField from "./RenderField";

class LoginForm extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit({ username, password }) {
    this.props.loginUser({ username, password });
  }

  render() {
    const { handleSubmit, fields: { username, password } } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit)} className="container">
        <Field
          name="username"
          type="text"
          component={RenderField}
          className="input"
          placeholder="Korisničko ime"
          validate={[required]}
          required
          {...username}
        />
        <Field
          name="password"
          type="password"
          component={RenderField}
          className="input"
          placeholder="Lozinka"
          validate={[required]}
          required
          {...password}
        />
        <button type="submit" className="button is-outlined">
          ULOGUJ SE
        </button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
  loginUser: PropTypes.func,
  onSubmit: PropTypes.func,
  error: PropTypes.string
};

const mapDispatchToProps = {
  loginUser
};

const rForm = reduxForm({
  form: "login",
  fields: ["username", "password"]
})(LoginForm);

export default connect(null, mapDispatchToProps)(rForm);
