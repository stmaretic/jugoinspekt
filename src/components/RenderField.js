import React from "react";

const RenderField = ({
  input,
  className,
  placeholder,
  type,
  meta: { touched, error }
}) => {
  const inputClassName = touched && error ? "input is-danger" : className;

  return (
    <div className="field">
      <div className="control">
        <input
          {...input}
          className={inputClassName}
          type={type}
          placeholder={placeholder}
        />
        {touched &&
          error && (
            <span
              className="has-text-danger"
              style={{ display: "inline-block", margin: "4px 20px" }}
            >
              {error}
            </span>
          )}
      </div>
    </div>
  );
};

export default RenderField;
