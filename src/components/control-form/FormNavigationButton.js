import React from "react";

const FormNavigationButton = props => {
  const className = `fas fa-angle-double-${props.pos}`;
  const buttonStyle = {
    margin: "0 6px"
  };
  return (
    <a style={buttonStyle} onClick={props.onClick}>
      <i className={className} />
    </a>
  );
};

export default FormNavigationButton;
