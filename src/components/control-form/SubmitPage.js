import React from "react";
import { reduxForm } from "redux-form";

import SubmitPageList from "./SubmitPageList";
import ControlPageButton from "./ControlPageButton";

const SubmitPage = props => {
  const { handleSubmit } = props;
  return (
    <form className="container is-fluid" onSubmit={handleSubmit}>
      <button className="button" type="submit">
        Zatvori lokaciju
      </button>
      <SubmitPageList redirectSubmitPage={props.redirectSubmitPage} />
      <ControlPageButton onClick={props.firstPage} />
    </form>
  );
};

const rForm = reduxForm({
  form: "control",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(SubmitPage);

export default rForm;
