import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ControlFormFirstPage from "./ControlFormFirstPage";
import ControlFormSecondPage from "./ControlFormSecondPage";
import SubmitPage from "./SubmitPage";
import {
  nextPage,
  prevPage,
  redirectSubmitPage,
  firstPage
} from "../../actions/pages";

class ControlForm extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    console.log(this.props.controlForm.values);
  }

  componentWillReceiveProps() {
    console.log(this.props.form);
  }

  render() {
    const {
      page,
      nextPage,
      prevPage,
      redirectSubmitPage,
      firstPage,
      fields,
      currentMeasurementPage,
      form
    } = this.props;
    return (
      <div className="container">
        <div className="columns">
          {page === 1 && <ControlFormFirstPage onSubmit={nextPage} />}
          {this.props.measurements &&
            page >= 2 && (
              <ControlFormSecondPage
                form={form}
                onSubmit={e => {
                  e.preventDefault();
                  nextPage;
                }}
                firstPage={firstPage}
              />
            )}
          {page === 0 && (
            <SubmitPage firstPage={firstPage} onSubmit={this.onSubmit} />
          )}
        </div>
      </div>
    );
  }
}

ControlForm.propTypes = {
  measurements: PropTypes.array.isRequired,
  currentMeasurementPage: PropTypes.number.isRequired,
  onSubmit: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  measurements: state.lists.measurements,
  currentMeasurementPage: state.lists.currentMeasurementPage,
  page: state.pages.page,
  controlForm: state.form.control || null,
  form: state.lists.formId
  // fieldValues: state.form.control.values
});

const mapDispatchToProps = {
  nextPage,
  prevPage,
  redirectSubmitPage,
  firstPage
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlForm);
