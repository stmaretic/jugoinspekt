import React from "react";

const SubmitPageItem = props => {
  const { forms } = props;

  const kvPairs = Object.entries(forms);
  let i = 1;

  if (forms) {
    return kvPairs.map((form, index) => {
      console.log(form);
      const parameters = Object.entries(form[1].values);
      return parameters.map((field, index) => {
        return (
          <li key={index}>
            Parametar {i++}: {field[1]}
          </li>
        );
      });
      // return <li key={index}>{field[1].values}</li>;
    });
  }
  return null;
  // let i = 1;
  // if (fieldValues) {
  //   return kvPairs.map((field, index) => {
  //     if (!field[0].indexOf(`parameter${props.active}_`)) {
  //       console.log(`parameter${props.active}`);
  //       return (
  //         <li key={index}>
  //           Parametar {i++}: {field[1]}
  //         </li>
  //       );
  //     }
  //   });
  // }
};

export default SubmitPageItem;
