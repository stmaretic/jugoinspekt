import React, { Component } from "react";
import { compose } from "redux";
import PropTypes from "prop-types";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";
import { fetchLocation } from "../../actions/location";
import { addMoreInputs, addMeasurement } from "../../actions/lists";
import { redirectSubmitPage } from "../../actions/pages";
import ControlPageButton from "./ControlPageButton";

import FormNavigation from "./FormNavigation";
import RenderSubFields from "./RenderSubFields";

class ControlFormSecondPage extends Component {
  componentDidMount() {
    this.props.fetchLocation();
  }

  onClick(e) {
    e.preventDefault();
    this.props.addMeasurement();
  }

  render() {
    const { onSubmit, redirectSubmitPage, fields } = this.props;
    return (
      <div className="container">
        <h2 className="has-text-centered">PODEŠAVANJE KONTROLE</h2>
        <FormNavigation />
        <form onSubmit={onSubmit}>
          {fields.map(RenderSubFields)}
          <a style={{ display: "block" }} onClick={this.props.addMoreInputs}>
            + Još parametara
          </a>
          <button
            type="button"
            className="button is-outline"
            onClick={redirectSubmitPage}
          >
            SAČUVAJ
          </button>
          <button
            type="submit"
            onClick={this.onClick.bind(this)}
            className="button is-outline"
          >
            NOVO MERENJE
          </button>
          <ControlPageButton onClick={this.props.firstPage} />
          {/* <div className="is-pulled-right">
            <a className="button is-outline" onClick={this.props.firstPage}>
              <i className="fas fa-cog" />Podešavanje kontrole
            </a>
          </div> */}
        </form>
      </div>
    );
  }
}

ControlFormSecondPage.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  redirectSubmitPage: PropTypes.func.isRequired,
  fields: PropTypes.array.isRequired,
  currentMeasurementPage: PropTypes.number.isRequired,
  fetchLocation: PropTypes.func.isRequired,
  addMoreInputs: PropTypes.func.isRequired,
  addMeasurement: PropTypes.func.isRequired
};

const rForm = reduxForm({
  // form is passed through props
  // fields are passed through props
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(ControlFormSecondPage);

const mapStateToProps = state => ({
  currentMeasurementPage: state.lists.currentMeasurementPage,
  fields: state.lists.measurements[state.lists.currentMeasurementPage],
  form: state.lists.formId
});

const mapDispatchToProps = {
  fetchLocation,
  addMoreInputs,
  addMeasurement,
  redirectSubmitPage
};

export default connect(mapStateToProps, mapDispatchToProps)(rForm);
