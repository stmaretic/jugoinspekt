import React from "react";
import { Field } from "redux-form";
import RenderField from "../RenderField";
import { required } from "../../formValidation";
import DeleteButton from "../DeleteButton";
import RenderFieldWithDelete from "../RenderFieldWithDelete";
import RenderFields from "../RenderFields";

const RenderSubFields = (name, index, fields) => {
  return (
    <Field
      name={name}
      type="text"
      placeholder={`Parametar ${index + 1}`}
      className="input"
      index={index}
      key={index}
      component={RenderFieldWithDelete}
      // validate={[required]}
    />
  );
};

export default RenderSubFields;
