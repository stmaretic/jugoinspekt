import React, { Component } from "react";
import { connect } from "react-redux";
import { redirectSubmitPage } from "../../actions/pages";
import {
  gotoMeasurement,
  dropdownMeasurement
} from "../../actions/measurements";

import SubmitPageItem from "./SubmitPageItem";

class SubmitPageList extends Component {
  renderValues() {}

  render() {
    const { active, forms } = this.props;
    return this.props.measurements.map((measurement, index) => {
      return (
        <div key={index}>
          <a
            style={{ display: "block" }}
            onClick={() => this.props.dropdownMeasurement(index)}
          >
            {`Merenje ${index + 1}`}
          </a>
          {active === index && (
            <div>
              <ul>
                <hr />
                <SubmitPageItem active={active} forms={forms} />
              </ul>
              <hr />
              <a onClick={() => this.props.gotoMeasurement(index)}>Izmeni</a>
              <hr />
            </div>
          )}
        </div>
      );
    });
  }
}

const mapStateToProps = state => ({
  measurements: state.lists.measurements,
  active: state.measurements.activeDropdownMeasurement,
  forms: state.form
});

const mapDispatchToProps = {
  redirectSubmitPage,
  gotoMeasurement,
  dropdownMeasurement
};

export default connect(mapStateToProps, mapDispatchToProps)(SubmitPageList);
