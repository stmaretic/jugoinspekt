import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import DatePicker from "react-datepicker";
import moment from "moment";
import { connect } from "react-redux";
import { required } from "../../formValidation";
import "react-datepicker/dist/react-datepicker.css";

import RenderField from "../RenderField";

class ControlFormFirstPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment()
    };

    this.onChange = this.onChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }

  onChange(date) {
    this.setState({
      startDate: date
    });
  }

  onSelect() {}

  render() {
    const { onSubmit } = this.props;
    return (
      <div className="container">
        <h2 className="has-text-centered">Kontrola</h2>
        <form onSubmit={onSubmit}>
          <div className="columns">
            <div className="column">
              <Field
                name="entry"
                type="text"
                placeholder="Ulaz"
                className="input"
                component={RenderField}
                validate={[required]}
              />
            </div>
            <div className="column">
              <DatePicker
                selected={this.state.startDate}
                onChange={this.onChange}
                onSelect={this.onSelect}
                showTimeSelect
                dateFormat="LLL"
                className="input"
              />
            </div>
          </div>
          <Field
            name="controlID"
            type="text"
            placeholder="Broj kontrole"
            className="input"
            component={RenderField}
            validate={[required]}
          />
          <Field
            name="controlLocation"
            type="text"
            placeholder="Mesto kontrole"
            className="input"
            component={RenderField}
            validate={[required]}
          />
          <Field
            name="orderer"
            type="text"
            placeholder="Nalogodavac"
            className="input"
            component={RenderField}
            validate={[required]}
          />
          <Field
            name="product"
            type="text"
            placeholder="Proizvod"
            className="input"
            component={RenderField}
            validate={[required]}
          />
          <button type="submit" className="button is-outline">
            DALJE
          </button>
        </form>
      </div>
    );
  }
}

ControlFormFirstPage.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

const rForm = reduxForm({
  form: "control",
  fields: [
    "entry",
    "date",
    "controlID",
    "controlLocation",
    "orderer",
    "product"
  ],
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(ControlFormFirstPage);

export default connect()(rForm);
