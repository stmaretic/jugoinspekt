import { FETCH_LOCATION } from "./actionTypes";

export const fetchLocation = () => {
  return dispatch => {
    if ("geolocation" in navigator) {
      return navigator.geolocation.getCurrentPosition(position => {
        dispatch({
          type: FETCH_LOCATION,
          payload: position
        });
      });
    }
  };
};
