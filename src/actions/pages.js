import {
  FORM_REDIRECT_SUBMIT_PAGE,
  NEXT_PAGE,
  PREV_PAGE,
  FIRST_PAGE
} from "./actionTypes";

export const redirectSubmitPage = () => {
  return dispatch => {
    dispatch({
      type: FORM_REDIRECT_SUBMIT_PAGE
    });
  };
};

export const nextPage = () => {
  return dispatch => {
    dispatch({
      type: NEXT_PAGE
    });
  };
};

export const prevPage = () => {
  return dispatch => {
    dispatch({
      type: PREV_PAGE
    });
  };
};

export const firstPage = () => {
  return dispatch => {
    dispatch({
      type: FIRST_PAGE
    });
  };
};
