import {
  DROPDOWN_MEASUREMENT,
  GOTO_MEASUREMENT,
  SAVE_MEASUREMENT_DATA
} from "./actionTypes";

export const dropdownMeasurement = index => {
  return dispatch => {
    dispatch({
      type: DROPDOWN_MEASUREMENT,
      payload: index
    });
  };
};

export const gotoMeasurement = index => {
  return dispatch => {
    dispatch({
      type: GOTO_MEASUREMENT,
      payload: index
    });
  };
};

export const saveMeasurement = data => {
  return dispatch => {
    dispatch({
      type: SAVE_MEASUREMENT_DATA,
      payload: data
    });
  };
};
