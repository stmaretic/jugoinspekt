import {
  ADD_MORE_INPUTS,
  REMOVE_INPUT,
  ADD_MEASUREMENT,
  PAGE_NEXT,
  PAGE_BACK,
  SAVE_CONTROL_FORM_DATA
} from "./actionTypes";

export const addMeasurement = () => {
  return (dispatch, getState) => {
    // let formId = action.payload.split("-")[2];
    //
    let formId = getState().lists.formId.split("-")[2];
    const newId = "control-measurements-" + ++formId;
    dispatch({
      type: ADD_MEASUREMENT,
      payload: newId
    });
  };
};

export const addMoreInputs = () => {
  return (dispatch, getState) => {
    const measurements = getState().lists.measurements;
    const currentMeasurementPage = getState().lists.currentMeasurementPage;
    const inputs = Object.entries(measurements[currentMeasurementPage]);
    const length = measurements[currentMeasurementPage].length;
    const checkInputs = () => {
      if (inputs.length != 0) {
        return Number(inputs[length - 1][1].split("_")[1]) + 1;
      }
      return 1;
    };
    dispatch({
      type: ADD_MORE_INPUTS,
      payload: "parameter_" + checkInputs()
    });
  };
};

export const removeInput = id => {
  return dispatch => {
    dispatch({
      type: REMOVE_INPUT,
      id
    });
  };
};

export const pageNext = () => {
  return dispatch => {
    dispatch({
      type: PAGE_NEXT
    });
  };
};

export const pageBack = () => {
  return dispatch => {
    dispatch({
      type: PAGE_BACK
    });
  };
};

export const saveControlData = data => {
  return dispatch => {
    dispatch({
      type: SAVE_CONTROL_FORM_DATA,
      payload: data
    });
  };
};
