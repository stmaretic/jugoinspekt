import {
  LOGIN_REQUEST,
  LOGIN_SUCCESSFUL,
  LOGOUT_USER,
  LOGOUT_SUCCESSFUL,
  AUTH_ERROR,
  REDIRECT_UNAUTHORIZED_USER,
  REDIRECT_SUCCESSFUL,
  REDIRECT_AUTH_USER
} from "./actionTypes";
import history from "../history";

const loginURL = `http://dev.jugoinspekt.devbox21.com/user/login?_format=json`;
const logoutURL = `http://dev.jugoinspekt.devbox21.com/user/logout?_format=json`;

export const loginUser = ({ username, password }) => {
  return async dispatch => {
    const request = new Request(`${loginURL}`, {
      method: "POST",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        name: username,
        pass: password
      })
    });

    try {
      const response = await fetch(request).then(res => {
        return res;
      });

      if (response && response.ok) {
        const user = await response.json();

        //  First dispatch function handles dispatching action to the API server,
        //  returns the token to global state.
        if (user) {
          dispatch({
            type: LOGIN_REQUEST,
            completed: Boolean,
            payload: user
          });

          //  After first dispatch is done, a second one is fired to redirect
          //  the user and forbid further login access before logout.
          dispatch({
            type: LOGIN_SUCCESSFUL,
            completed: Boolean
          });
          history.push("/");
        } else {
          console.log("Unsuccessful request.");
        }
      }
    } catch (error) {
      console.log(error.toString());
      dispatch(authError(error));
    }
  };
};

export const logoutUser = token => {
  return async dispatch => {
    const request = new Request(`${logoutURL}`, {
      method: "GET",
      headers: new Headers({
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-CSRF-Token": token
      })
    });

    try {
      const response = await fetch(request).done();
      console.log(response);
    } catch (error) {
      console.log(error.toString());
    }

    dispatch({
      type: LOGOUT_USER,
      completed: Boolean,
      payload: token
    });

    dispatch({
      type: LOGOUT_SUCCESSFUL,
      completed: Boolean
    });
    history.push("/");
  };
};

export const authError = error => {
  return {
    type: AUTH_ERROR,
    payload: error
  };
};

export const redirectUnauthUser = () => {
  return async dispatch => {
    try {
      await dispatch({
        type: REDIRECT_UNAUTHORIZED_USER
      });
      history.push("/login");

      await dispatch({
        type: REDIRECT_SUCCESSFUL
      });
    } catch (error) {
      console.log(error.toString());
    }
  };
};

export const redirectAuthUser = () => {
  return async dispatch => {
    try {
      await dispatch({
        type: REDIRECT_AUTH_USER
      });
      history.push("/");

      await dispatch({
        type: REDIRECT_SUCCESSFUL
      });
    } catch (error) {
      console.log(error.toString());
    }
  };
};
