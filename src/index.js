import React from "react";
import ReactDOM from "react-dom";
import { Switch, Router, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { store, persistor } from "./configureStore";
import registerServiceWorker from "./registerServiceWorker";
import "bulma/css/bulma.css";
import history from "./history";

import App from "./App";
import AuthRequired from "./containers/AuthRequired";
import RestrictedAccess from "./containers/RestrictedAccess";
import LoginPage from "./components/pages/LoginPage";
import Navbar from "./components/navbar/Navbar";
import ControlPage from "./components/pages/ControlPage";

const Root = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router history={history}>
          <div>
            <Route component={Navbar} />
            <Switch>
              <Route path="/control" component={AuthRequired(ControlPage)} />
              <Route path="/login" component={RestrictedAccess(LoginPage)} />
              <Route path="/logout" component={AuthRequired(App)} />
              <Route path="/" component={App} />
            </Switch>
          </div>
        </Router>
      </PersistGate>
    </Provider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));
registerServiceWorker();
